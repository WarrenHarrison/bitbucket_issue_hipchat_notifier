require 'rubygems'
require 'bundler/setup'
require 'slack-notifier'
Bundler.require

if File.exists?('settings.yml')
  SETTINGS = YAML.load_file('settings.yml')
else
  abort('cannot find a settings.yml file')
end

timestamp = Time.now.getutc.to_i
if File.exists?('lastrun')
  last_timestamp = File.open('lastrun', 'rb').read.to_i
else
  last_timestamp = timestamp
end
File.open('lastrun', 'w').write(timestamp)

bb = BitBucket.new :basic_auth => "#{SETTINGS['bb_user']}:#{SETTINGS['bb_password']}"
hc = HipChat::Client.new(SETTINGS['hc_token'])

if SETTINGS['slack_team'] && SETTINGS['slack_token']
  slack = Slack::Notifier.new(
    SETTINGS['slack_team'],
    SETTINGS['slack_token'],
    :channel => '#developers',
    :username => 'Bitbucket Issues'
  )
end

repos = bb.repos.list SETTINGS['bb_account']
# repos = [].push(bb.repos.get(SETTINGS['bb_user'], 'issuenotifier'))
repos.each do |r|
  # puts "checking repo #{r.name}" # DEBUG
  r_updated = Time.parse(r.utc_last_updated).getutc.to_i
  if r.has_issues
    # GET ISSUES
    issues = begin
      bb.issues.list_repo(SETTINGS['bb_account'],r.slug)
    rescue BitBucket::Error::NotFound
      []
    end
    issues.each do |i|
      # puts "checking issue #{i.title}" # DEBUG
      i_created = Time.parse(i.utc_created_on).getutc.to_i
      i_updated = Time.parse(i.utc_last_updated).getutc.to_i
      # IS THIS A NEW ISSUE?
      if i_updated > last_timestamp
        if i_created == i_updated
          message = "<strong>#{i.reported_by.display_name}</strong> created <a href=\"https://bitbucket.org/#{SETTINGS['bb_account']}/#{r.slug}/issue/#{i.local_id}\">#{i.metadata.kind.upcase} ##{i.local_id}: #{i.title}</a> [assigned to #{i.responsible.display_name}]"
        else
          message = "<a href=\"https://bitbucket.org/#{SETTINGS['bb_account']}/#{r.slug}/issue/#{i.local_id}\">#{i.metadata.kind.upcase} ##{i.local_id}: #{i.title}</a> was updated [status: #{i['status']}] [assignee #{i.responsible.display_name}]"
        end
        # SEND ISSUE MESSAGE TO HIPCHAT
        # puts message # DEBUG
        hc[SETTINGS['hc_room']].send('Bitbucket', message, { :color => 'gray' })
        slack.ping message if slack
        #GET COMMENTS
        if i.comment_count > 0
          bb.issues.comments.all(SETTINGS['bb_account'],r.slug,i.local_id).each do |c|
            c_updated = Time.parse(c.utc_updated_on).getutc.to_i
            if c_updated > last_timestamp
              message = "<strong>#{c.author_info.display_name}</strong> commented: &ldquo;#{c.content}&rdquo; on <a href=\"https://bitbucket.org/#{SETTINGS['bb_account']}/#{r.slug}/issue/#{i.local_id}\">#{SETTINGS['bb_account']}/#{r.slug}/issue/#{i.local_id}</a>"
              # SEND COMMENT TO HIPCHAT
              # puts message # DEBUG
              hc[SETTINGS['hc_room']].send('Bitbucket', message, { :color => 'gray' })
              slack.ping message if slack
            end
          end
        end
      end

    end
  end
end