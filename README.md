Your Bitbucket Issues & Comments in your Hipchat
================================================

## What?

Hipchat & Bitbucket work nicely together when it comes to repositories and commits. What about those second-class citizens, Bitbucket Issues. They are all but ignored. This rather simple & crude script will allow you to poll your repos for new or updated issues and comments and send notifications to a Hipchat room of your choice.

## How?

```
git clone git@bitbucket.org:WarrenHarrison/bitbucket_issue_hipchat_notifier.git
cd bitbucket_issue_hipchat_notifier
bundle install
cp settings.sample.yml settings.yml
```

Edit the variables at the top of the `settings.yml` and set as desired

```ruby
bb_account: 'YOUR_ACCOUNT_OR_TEAM_NAME'
bb_user: 'YOUR_READONLY_USER_NAME'
bb_password: 'YOUR_READONLY_USER_PASSWORD'
hc_token: 'YOUR_HIPCHAT_NOTIFICATION_TOKEN' # https://hipchat.com/admin/api
hc_room: 'YOUR_HIPCHAT_ROOM_NAME'
```

you can optionally add settings for slack.com to settings.yml

```ruby
slack_team: 'YOUR_SLACK_TEAM'
slack_token: 'YOUR_SLACK_TOKEN'
```

Since this just using basic authentication, it is recommended you create a new, read-only user for your team. That way, if your credentials are revealed, the worst is a peek at your sourcecode and no changes can be made.

Once variables are set as desired, _prime the pump_ by running the script
`ruby issue_watcher.rb`

You should see a new `lastrun` file appear. This is simple storing a timestamp of the last time the script checked for issue/comment updates.

To give it a proper test, add an new issue or comment to one of your repos and run the script again
`ruby issue_watcher.rb`

You should see a nice notification display in your chosen Hipchat room. Huzzah!

## and then... ?

Running this manually is silly, so set it up as a nice cronjob

```
crontab -e

0,10,20,30,40,50 * * * * /usr/bin/ruby /home/wph/bitbucket_issue_hipchat_notifier/issue_watcher.rb
# this will run it every 10 minutes
```

_note that you will have to adjust your paths to ruby and your home in the sample cronjob above_

## Issues

Well, this is horribly inefficient as it checks **every** issue for **every** repository each time it runs. This is because issues do not affect a repo's last update time. _What a pickle!_ It might be nicer to store each repo's issue/comment counts and update times locally. That sounds like a great idea for another time.